package com.javatopic.minhbdk.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity(name = "Movies")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private int time;

    @Column(columnDefinition = "DATE", nullable = false)
    private Date openDay;

    private String language;
    private String actor;
    private String nation;
    private String producer;

    @Column(columnDefinition = "TEXT")
    private String summary;

    private String status;
    private boolean isDeleted;

    @ManyToOne
    private MovieType movieType;

    @OneToMany(mappedBy = "movie")
    private Set<ShowTime> showTimeSet;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Date getOpenDay() {
        return openDay;
    }

    public void setOpenDay(Date openDay) {
        this.openDay = openDay;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public void setMovieType(MovieType movieType) {
        this.movieType = movieType;
    }

    public Set<ShowTime> getShowTimeSet() {
        return showTimeSet;
    }

    public void setShowTimeSet(Set<ShowTime> showTimeSet) {
        this.showTimeSet = showTimeSet;
    }
}
