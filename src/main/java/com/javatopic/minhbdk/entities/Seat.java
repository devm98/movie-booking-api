package com.javatopic.minhbdk.entities;

import javax.persistence.*;

@Entity(name = "Seats")
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String rowPosition;
    private int colPosition;

    private int isSelected;

    @ManyToOne
    private SeatType seatType;

    @ManyToOne
    private MovieTheater movieTheater;

    public Long getId() {
        return id;
    }

    public String getRowPosition() {
        return rowPosition;
    }

    public void setRowPosition(String rowPosition) {
        this.rowPosition = rowPosition;
    }

    public int getColPosition() {
        return colPosition;
    }

    public void setColPosition(int colPosition) {
        this.colPosition = colPosition;
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public void setSeatType(SeatType seatType) {
        this.seatType = seatType;
    }
}
