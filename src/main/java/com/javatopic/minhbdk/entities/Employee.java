package com.javatopic.minhbdk.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "Employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String fullName;

    @Column(nullable = false)
    private String password;

    private String identityCard;

    @Column(nullable = false)
    private String email;

    private String address;

    @Column(columnDefinition = "DATE", nullable = false)
    private Date birthday;

    private int gender;

    private Date TimeWorkStart;

    private int isDoing;

    @ManyToOne
    private Role role;

    @OneToMany(mappedBy = "employee")
    private List<Ticket> ticketList;

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Date getTimeWorkStart() {
        return TimeWorkStart;
    }

    public void setTimeWorkStart(Date timeWorkStart) {
        TimeWorkStart = timeWorkStart;
    }

    public int getIsDoing() {
        return isDoing;
    }

    public void setIsDoing(int isDoing) {
        this.isDoing = isDoing;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
